#!/bin/bash

#============================================================================================
#
# Helper for restoring a production install (not Docker) of PeerTube to a previous version/backup
# 
# Assume using:
# * nginx
# * systemd, service unit: peertube
# * user: peertube, install in $HOME=/var/www/peertube
# * postgres, database: peertube_prod
#
#============================================================================================

usage () {
    echo "Usage: restore_peertube.sh"
    echo ""
    echo "Restore PeerTube to previous version/backup"
    echo ""
    echo "  -h --help                   Show this help"
    echo "  -V --version                Path to previous version (REQUIRED)"
    echo "  -b --backup                 Path to PostgreSQL backup file (REQUIRED)"
    echo "  -y --yes                    Assume yes for confirmation of install"
    return
}

while [[ -n "$1" ]]; do
    case "$1" in
        -c | --check-only)  check=1
                            ;;
        -y | --yes)         yes=1
                            ;;
        -V | --version)     shift 
                            version_path="$1"
                            ;;
        -b | --backup)      shift 
                            backup_path="$1"
                            ;;
        -h | --help)        usage
                            exit
                            ;;
        *)                  usage >&2
                            exit 1
                            ;;
    esac
    shift
done

PTUSER=peertube
PTHOME=/var/www/peertube
PTLATEST="$PTHOME/peertube-latest"
DBNAME=peertube_prod

if [[ $EUID  != 0 ]]; then
    echo "ERROR: Restore requires script to be run with sudo"
    exit 1
fi

if [[ -z "$backup_path" || -z "$version_path" ]]; then
    usage
    exit 1
fi

restore_postgres () {
    # clean+create --> drop and recreate database
    # -d postgres --> just need db to connect to (not restore target)
    su postgres -c "pg_restore --clean --create -d postgres $backup_path"
    echo "Restored database"
    return
}

if [[ ! -n $yes ]]; then
    echo "Database $DBNAME will be overwritten and restored to previous version"
    read -p "Do you want to continue? [Y/n] " continue_restore
    shopt -s nocasematch
    if [[ ! (-z $continue_restore || $continue_restore =~ ^y(es)?) ]]; then
        echo "Aborting"
        exit 0
    fi
    shopt -u nocasematch
    echo "Continuing"
fi


# Start of restore actions
systemctl stop peertube

restore_postgres

# update peertube-latest, install yarn dependencies (if needed) and update defaults
rm -rf "$PTLATEST"
su "$PTUSER" -c "ln -s $version_path $PTLATEST"
su "$PTUSER" -c "yarn install --production --pure-lockfile --cwd $PTLATEST"
su "$PTUSER" -c "cp $PTLATEST/config/default.yaml $PTHOME/config/default.yaml"

systemctl start peertube
# End of upgrade actions

echo "Restore complete!"
echo "You may need to manually delete orphaned storage files if created since upgrade"
