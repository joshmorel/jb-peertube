#!/bin/bash

#============================================================================================
#
# Helper for upgrading production install (not Docker) of PeerTube
# 
# Assume using:
# * nginx
# * systemd, service unit: peertube
# * user: peertube, install in $HOME=/var/www/peertube
# * postgres, database: peertube_prod, user: peertube
#
#============================================================================================

usage () {
    echo "Usage: upgrade_peertube.sh"
    echo ""
    echo "Upgrade PeerTube to the latest version"
    echo ""
    echo "  -h --help                   Show this help"
    echo "  -c --check-only             Check for latest version only, don't install"
    echo "  -y --yes                    Assume yes for confirmation of install"
    return
}

while [[ -n "$1" ]]; do
    case "$1" in
        -c | --check-only)  check=1
                            ;;
        -y | --yes)         yes=1
                            ;;
        -h | --help)        usage
                            exit
                            ;;
        *)                  usage >&2
                            exit 1
                            ;;
    esac
    shift
done

PTUSER=peertube
PTHOME=/var/www/peertube
PTLATEST="$PTHOME/peertube-latest"
DBNAME=peertube_prod
SQL_BACKUP_PATH="$PTHOME/backup/sql-peertube_prod-$(date +"%Y%m%d-%H%M").bak"

# confirm already properly installed
if [[ ! -d "$PTLATEST/node_modules" ]]; then
    echo "ERROR: PeerTube not installed, expecting $PTLATEST/node_modules directory"
    exit 1
fi
if [[ $(systemctl is-enabled peertube 2>/dev/null) != "enabled" ]]; then
    echo "ERROR: PeerTube not properly installed, expecting enabled peertube systemd service"
    exit 1
fi

get_current_version () {
    echo v$(grep version "$PTLATEST/package.json" | cut -d '"' -f 4)
    return
}

get_latest_version () {
    echo $(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4)
    return
}

backup_postgres () {
    su "$PTUSER" -c "mkdir -p $PTHOME/backup"
    su postgres -c "pg_dump -F c $DBNAME" > $SQL_BACKUP_PATH
    echo "Backed-up database to $SQL_BACKUP_PATH"
    return
}

current_version=$(get_current_version)
latest_version=$(get_latest_version)
if [[ $current_version == $latest_version ]]; then
    echo "Local PeerTube is NOT upgrade candidate"
    echo "Current and latest versions are: $current_version"
    exit 0
fi

echo "Current version: $current_version"
echo "Latest version: $latest_version"
echo "Local PeerTube is upgrade candidate"

if [[ -n $check ]]; then
    exit 0
fi

if [[ $EUID  != 0 ]]; then
    echo "ERROR: Upgrade requires script to be run with sudo"
    exit 1
fi

if [[ ! -n $yes ]]; then
    read -p "Do you want to continue? [Y/n] " continue_install
    shopt -s nocasematch
    if [[ ! (-z $continue_install || $continue_install =~ ^y(es)?) ]]; then
        echo "Aborting"
        exit 0
    fi
    shopt -u nocasematch
    echo "Continuing"
fi


# Start of upgrade actions
systemctl stop peertube

backup_postgres

# download and extract
wget -q "https://github.com/Chocobozzz/PeerTube/releases/download/${latest_version}/peertube-${latest_version}.zip" -O /tmp/peertube-${latest_version}.zip
if [[ $? -ne 0 ]]; then
  echo "ERROR: download failed for PeerTube $latest_version"
  exit $?
fi
su "$PTUSER" -c "unzip /tmp/peertube-${latest_version}.zip -d $PTHOME/versions"
rm /tmp/peertube-${latest_version}.zip
echo "Downloaded and extracted PeerTube $latest_version"

# update peertube-latest, install yarn dependencies and update defaults
rm -rf "$PTLATEST"
su "$PTUSER" -c "ln -s $PTHOME/versions/peertube-${latest_version} $PTLATEST"
su "$PTUSER" -c "yarn install --production --pure-lockfile --cwd $PTLATEST"
su "$PTUSER" -c "cp $PTLATEST/config/default.yaml $PTHOME/config/default.yaml"

systemctl start peertube
# End of upgrade actions

echo "Upgrade complete!"
echo "Note differences in config files:"

echo ""
echo "production.yaml.example"
echo "--------------------------------------------"
diff "$PTLATEST/config/production.yaml.example" "$PTHOME/versions/peertube-${current_version}/config/production.yaml.example"

echo ""
echo "nginx/peertube"
echo "--------------------------------------------"
diff "$PTLATEST/support/nginx/peertube" "$PTHOME/versions/peertube-${current_version}/support/nginx/peertube"

echo ""
echo "systemd/peertube.service"
echo "--------------------------------------------"
diff "$PTLATEST/support/systemd/peertube.service" "$PTHOME/versions/peertube-${current_version}/support/systemd/peertube.service"
